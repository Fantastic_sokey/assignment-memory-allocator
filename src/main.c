#include "mem_internals.h"
#include "mem.h"
#include "util.h"

#define INITIAL_SIZE 4196

static struct block_header *get_block_header(void *data);

bool test_1(){ // выделение памяти
    printf("Test 1:\n");
    void* block = _malloc(1000);
	if (!block) {
        printf("Test 1 failed.\n");
        return false;
    }
  	printf("Test 1 passed.\n");
  	_free(block);
    return true;
}

bool test_2(){ // освобождение одного блока из нескольких выделенных
    printf("Test 2:\n");
	void* block1 = _malloc(150);
	void* block2 = _malloc(150);
  	void* block3 = _malloc(150);
    if (!block1 || !block2 || !block3){
        printf("Test 2 failed. Can not allocate.\n"); // пустой блок памяти
        return false;
    }
    _free(block1);
    if (!get_block_header(block1)->is_free){
        printf("Test 2 failed. Can not allocate.\n"); // блок памяти не освобожден
        return false;
    }
    _free(block2);
	_free(block3);
    printf("Test 2 passed.\n");
	return true;
}

bool test_3(){ // освобождение двух блоков из нескольких выделенных
    printf("Test 3:\n");
    void* block1 = _malloc(250);
	void* block2 = _malloc(250);
  	void* block3 = _malloc(250);
    void* block4 = _malloc(250);
    if (!block1 || !block2 || !block3 || !block4){
        printf("Test 3 failed. Allocation and freeing failed. \n"); // пустой блок памяти
        return false;
    }
    _free(block1);
	_free(block2);
    if (!get_block_header(block1)->is_free || !get_block_header(block2)->is_free){
        printf("Test 3 failed. Allocation and freeing failed. \n"); // блоки памяти не освобождены
        return false;
    }
    _free(block3);
	_free(block4);
    printf("Test 3 passed.\n");
	return true;
}

bool test_4(){ // Память закончилась, новый регион памяти расширяет старый
    printf("Test 4:\n");
	void* block = _malloc(10500);
	if (block != ((int8_t*)HEAP_START) + offsetof(struct block_header, contents)) {
		printf("Test 4 failed.\n"); // регион памяти не увеличился
		return false;
	}
    _free(block);
	printf("Test 4 passed.\n");
	return true;
}

bool test_5(){ // Память закончилась, старый регион памяти не расширить из-за другого выделенного диапазона адресов, новый регион выделяется в другом месте
    printf("Test 5:\n");	
    (void)mmap(0, 300000, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_FIXED, 0, 0);
	void* block = _malloc(45000);
	if (block == (int8_t*)HEAP_START + offsetof(struct block_header, contents)) {
        printf("Test 5 failed\n"); // новый регион выделен за счет старого
		return false;
	}
	_free(block);
	printf("Test 5 passed.\n");
	return true;
}

static struct block_header *get_block_header(void *data) {
    return (struct block_header*)((uint8_t*)data - offsetof(struct block_header, contents));
}

int main() {
    if (test_1() && test_2() && test_3() && test_4() && test_5()) 
        printf("All tests passed successfully!");
    else 
        printf("One or more tests were not completed successfully. Try again.");
    return 0;
}
